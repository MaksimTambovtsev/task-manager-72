package ru.tsc.tambovtsev.tm.listener.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.tambovtsev.tm.listener.AbstractListener;
import ru.tsc.tambovtsev.tm.dto.Domain;

@Getter
@Setter
@Component
@NoArgsConstructor
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
    }

}
