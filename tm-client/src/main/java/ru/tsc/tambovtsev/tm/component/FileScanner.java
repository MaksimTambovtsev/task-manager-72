package ru.tsc.tambovtsev.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;
import ru.tsc.tambovtsev.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es =
            Executors.newSingleThreadScheduledExecutor();

    private Bootstrap bootstrap;

    @Nullable
    @Autowired
    private AbstractListener[] commands;

    private final List<String> commandsList = new ArrayList<>();

    private final File folder = new File("./");

    public FileScanner(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        final Iterable<AbstractListener> commandsList = Arrays.asList(commands);
        commandsList.forEach(c -> this.commandsList.add(c.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file: folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            final boolean checkFile = commandsList.contains(fileName);
            if (checkFile) {
                file.delete();
                bootstrap.getEventPublisher().publishEvent(new ConsoleEvent(fileName));
            }
        }
    }

    public void start() {
        init();
    }

}
