package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class UserEndpointTest {

    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();
    private IUserEndpoint userEndpoint = IUserEndpoint.newInstance();
    private String token = null;

    @Before
    public void createUser() {
        final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        token = userLoginResponse.getToken();
        userEndpoint.registryUser(new UserRegistryRequest(token, "unitTest", "unitTest", "wqert@rewq.com"));
    }

    @Test
    public void registryUserTest() {
        UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("unitTest", "unitTest")
        );
        Assert.assertNotNull(userEndpoint.viewProfileUser(new UserProfileRequest(userLoginResponse.getToken())).getUser().getLogin()
        );
    }

    @Test
    public void profileUserTest() {
        Assert.assertNotNull(userEndpoint.viewProfileUser(new UserProfileRequest(token)).getUser().getLogin()
        );
    }

    @Test
    public void changeUserPasswordTest() {
        UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("unitTest", "unitTest")
        );
        userEndpoint.changeUserPassword(new UserChangePasswordRequest(userLoginResponse.getToken(), "wsw"));
        UserLoginResponse userLoginResponseAfter = authEndpoint.login(
                new UserLoginRequest("unitTest", "wsw")
        );
        Assert.assertNotNull(userEndpoint.viewProfileUser(new UserProfileRequest(userLoginResponseAfter.getToken())).getUser().getLogin()
        );
    }

    @Test
    public void updateUserProfileTest() {
        UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("unitTest", "unitTest")
        );
        userEndpoint.updateUserProfile(new UserUpdateProfileRequest(userLoginResponse.getToken(), "qwer", "123123", "rweqwe"));
        Assert.assertNotNull(userEndpoint.viewProfileUser(new UserProfileRequest(userLoginResponse.getToken())).getUser().getFirstName());
        Assert.assertNotNull(userEndpoint.viewProfileUser(new UserProfileRequest(userLoginResponse.getToken())).getUser().getLastName());
        Assert.assertNotNull(userEndpoint.viewProfileUser(new UserProfileRequest(userLoginResponse.getToken())).getUser().getMiddleName());
    }

}
