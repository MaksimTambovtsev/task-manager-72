package ru.tsc.tambovtsev.tm.dto.logger;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@XmlRootElement
public class EntityLogDTO implements Serializable {

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private final Date date = new Date();

    @NotNull
    private final String className;

    @NotNull
    private final Object entity;

    @NotNull
    private final String type;

    private String table;

    public EntityLogDTO(@NotNull String className, @NotNull Object entity, @NotNull String type) {
        this.className = className;
        this.entity = entity;
        this.type = type;
    }
}