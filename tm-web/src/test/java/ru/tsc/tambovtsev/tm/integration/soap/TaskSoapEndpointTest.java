package ru.tsc.tambovtsev.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpointImpl;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpointImpl;
import ru.tsc.tambovtsev.tm.client.AuthSoapEndpointClient;
import ru.tsc.tambovtsev.tm.client.TaskSoapEndpointClient;
import ru.tsc.tambovtsev.tm.marker.IntegrationCategory;
import ru.tsc.tambovtsev.tm.model.Task;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class TaskSoapEndpointTest {

    @NotNull
    private static IAuthEndpointImpl authEndpoint;

    @NotNull
    private static ITaskEndpointImpl taskEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final Task task = new Task("Task1", "Description1");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").isValue());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull Map<String, List<String>> headers =
                CastUtils.cast(
                        (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
                );
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        taskEndpoint.save(task);
    }

    @After
    @SneakyThrows
    public void clean() {
        taskEndpoint.clear();
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Task> tasks = taskEndpoint.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Task taskFind = taskEndpoint.findById(task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskFind.getId(), task.getId());
    }

    @Test
    public void saveTest() {
        @NotNull final Task taskNew = new Task("Task2", "Description2");
        @NotNull final Task taskRes = taskEndpoint.save(taskNew);
        Assert.assertNotNull(taskRes);
        Assert.assertEquals(taskRes.getId(), taskNew.getId());
    }

    @Test
    public void deleteTest() {
        taskEndpoint.delete(task);
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void deleteAllTest() {
        taskEndpoint.deleteAll(Collections.singletonList(task));
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void clearTest() {
        taskEndpoint.clear();
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void deleteByIdTest() {
        taskEndpoint.deleteById(task.getId());
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, taskEndpoint.count());
    }
    
}
