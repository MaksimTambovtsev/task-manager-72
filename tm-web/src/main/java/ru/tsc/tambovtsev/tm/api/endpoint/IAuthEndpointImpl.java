package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.tambovtsev.tm.model.Result;
import ru.tsc.tambovtsev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpointImpl {

    @NotNull
    @WebMethod
    @PostMapping(value = "/login")
    Result login(
            @NotNull
            @WebParam(name = "login") final String login,
            @NotNull
            @WebParam(name = "password") final String password
    );

    @NotNull
    @WebMethod
    @GetMapping(value = "/profile")
    User profile();

    @NotNull
    @WebMethod
    @PostMapping(value = "/logout")
    Result logout();

}
