package ru.tsc.tambovtsev.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpointImpl;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectSoapEndpointClient {

    public static IProjectEndpointImpl getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/ProjectEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.tambovtsev.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectEndpointImpl result = Service.create(url, name).getPort(IProjectEndpointImpl.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
