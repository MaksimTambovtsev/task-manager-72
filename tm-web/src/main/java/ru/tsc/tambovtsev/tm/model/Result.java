package ru.tsc.tambovtsev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    private boolean value;

    private String message;

    public Result(boolean value) {
        this.value = value;
    }

    public Result(String message) {
        this.message = message;
    }

}
