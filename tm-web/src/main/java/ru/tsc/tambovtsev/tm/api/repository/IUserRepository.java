package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.tambovtsev.tm.model.User;

public interface IUserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@Nullable String login);

    void deleteByLogin(@Nullable String login);

}
