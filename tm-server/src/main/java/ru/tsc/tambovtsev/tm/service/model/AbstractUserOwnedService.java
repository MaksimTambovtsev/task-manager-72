package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IOwnerGraphRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IUserOwnedService;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.NameEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IOwnerGraphRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    public abstract IOwnerGraphRepository<M> getRepository();

    @Override
    @Transactional
    public void clear() {
        @NotNull final IOwnerGraphRepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final IOwnerGraphRepository<M> repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final SortTable sortTable
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sortTable).orElseThrow(NameEmptyException::new);
        @NotNull final IOwnerGraphRepository<M> repository = getRepository();
        final Sort sort = Sort.by(sortTable.name().toLowerCase(Locale.ROOT)).ascending();
        final Pageable pageable = PageRequest.of(0,30, sort);
        return repository.findAllByUserId(userId, pageable);
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final IOwnerGraphRepository<M> repository = getRepository();
        return repository.count();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IOwnerGraphRepository<M> repository = getRepository();
        @Nullable final M result = repository.findByUserIdAndId(userId, id);
        if (result == null) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IOwnerGraphRepository<M> repository = getRepository();
        return repository.findByUserIdAndId(userId, id);
    }

}
