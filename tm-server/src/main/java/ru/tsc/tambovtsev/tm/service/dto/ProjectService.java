package ru.tsc.tambovtsev.tm.service.dto;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;

import java.util.*;

@Service
public final class ProjectService extends AbstractUserOwnedService<ProjectDTO, IProjectRepository> implements IProjectService {

    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final ProjectDTO project = repository.findByUserIdAndId(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        project.setUserId(userId);
        repository.save(project);
        return project;
    }

}
