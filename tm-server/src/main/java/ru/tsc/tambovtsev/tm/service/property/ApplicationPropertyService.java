package ru.tsc.tambovtsev.tm.service.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.property.IApplicationPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class ApplicationPropertyService implements IApplicationPropertyService {

    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @Value("#{environment['developer']}")
    private String authorName;

    @Value("${environment['email']:mtambovtsev@t1-consulting.ru}")
    private String authorEmail;

}
