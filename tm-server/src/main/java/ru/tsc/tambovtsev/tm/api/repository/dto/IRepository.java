package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

@NoRepositoryBean
public interface IRepository<M extends AbstractEntityDTO> extends JpaRepository<M, String> {
}
