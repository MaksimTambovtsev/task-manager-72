package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractEntityDTO> {

    @Nullable
    List<M> findAll();

    void addAll(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void create(@Nullable M model);

}

